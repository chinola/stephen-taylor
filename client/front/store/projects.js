import { modelBase, addOrMerge } from './index';
import { isObject } from 'util';


export default modelBase({
  baseURL: 'project',
  order: 'sort ASC',
  getters: {
    next(state, getters) {
      return (currID) => {
        const currIdx = getters.all.findIndex((p) => p.id === currID);
        let next = getters.all[currIdx + 1];
        if (!next) {
          next = getters.all[0]
        }
        return next;
      };
    },
  },
  mutations: {
    // override this, because the way the extra images are stored has changed
    setAll(state, models) {
      models.forEach((model) => {
        if (model.extraImages.length > 0) {
          if (!isObject(model.extraImages[0])) {
            model.extraImages = model.extraImages.map((v) => ({id: v}));
          }
        }
        addOrMerge(state.all, model);
      });
      state.count = models.length;
    },
    setOne(state, model) {
      if (model.extraImages.length > 0) {
        if (!isObject(model.extraImages[0])) {
          model.extraImages = model.extraImages.map((v) => {id: v});
        }
      }
      addOrMerge(state.all, model);
      state.count = state.all.length;
    }
  }
});
