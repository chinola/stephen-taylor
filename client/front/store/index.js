import Vue from 'vue';
import Vuex from 'vuex';

export const addOrMerge = (arr, model, key = 'id') => {
  const idx = arr.findIndex((m) => m[key] === model[key]);
  if (idx > -1) {  // replace existing
    Vue.set(arr, idx, Object.assign(arr[idx], model));
  } else { // add new
    arr.push(model);
  }
};

// default options for setup
const defaultOptions = {
  baseURL: undefined,
  limit: 50,
  order: 'id ASC',
};

export const modelBase = (options = {}) => {
  if (!options.baseURL) {
    throw new TypeError('Missing baseURL');
  }

  options = Object.assign({}, defaultOptions, options);

  // default options for fetching
  const defaultOpts = {
    filter: {
      limit: options.limit,
      order: options.order,
    },
  };

  return {
    /*
    ** Common state
    */
    state() {
      return Object.assign({
        all: [],
        count: null,
        total: null,
      }, options.state);
    },

    /*
    ** Common getters
    */
    getters: Object.assign({
      all(state) {
        return [...state.all];
      },
      count(state) {
        return state.count;
      },
      total(state) {
        return state.total;
      },
      getByKey(state) {
        return (key, value) => {
          return state.all.find((m) => m[key] === value);
        };
      },
    }, options.getters),

    /*
    ** Common mutations
    */
    mutations: Object.assign({
      setAll(state, models) {
        models.forEach((model) => {
          addOrMerge(state.all, model);
        });
        state.count = models.length;
      },
      setOne(state, model) {
        addOrMerge(state.all, model);
        state.count = state.all.length;
      },
      delOne(state, id) {
        const idx = state.all.findIndex((m) => m.id === id);
        if (idx > -1) {
          return state.all.splice(idx, 1)[0];
        }
      },
      setTotal(state, val) {
        state.total = val;
      },
      setSort(state, fn) {
        state.sort = fn;
      },
    }, options.mutations),

    /*
    ** Common actions
    */
    actions: Object.assign({
      async count(ctx, opts = {}) {
        const resp = await this.$axios.get(`/${options.baseURL}/count/`);
        ctx.commit('setTotal', resp.data.count);
      },
      async fetch(ctx, opts = defaultOpts) {
        if (!ctx.state.total || opts.forceCount) {
          await ctx.dispatch('count', opts);
        }
        let resp;
        if (parseInt(opts.id)) {  // get one by id
          resp = await this.$axios.get(`/${options.baseURL}/${opts.id}/`);
          ctx.commit('setOne', resp.data);
        } else if (opts.filter.limit === 1) {  // get one by filter
          resp = await this.$axios.get(`/${options.baseURL}/findOne/`, {
            params: {
              filter: opts.filter,
            },
          });
          ctx.commit('setOne', resp.data);
        } else {  // get all in filter
          resp = await this.$axios.get(`/${options.baseURL}/`, {
            params: {
              filter: opts.filter,
            },
          });
          ctx.commit('setAll', resp.data);
        }
      },
      async save(ctx, model) {
        let method = this.$axios.put;
        let url = `${options.baseURL}/${model.id}/`;
        if ([undefined, null, ''].includes(model.id)) {
          method = this.$axios.post;
          url = `${options.baseURL}/`;
        }
        const resp = await method(url, model);
        ctx.commit('setOne', resp.data);
      },
      async update(ctx, model) {
        const resp = await this.$axios.patch(
          `${options.baseURL}/${model.id}/`, model
        );
        ctx.commit('setOne', resp.data);
      },
      async remove(ctx, id) {
        const url = `${options.baseURL}/${id}/`;
        await this.$axios.delete(url);
        ctx.commit('delOne', id);
      },
    }, options.actions),
  };
};


export default {
  actions: {
    async nuxtServerInit({ dispatch }) {
      try {
        await dispatch('pages/fetch');
        await dispatch('projects/fetch');
      } catch (e) {
        console.error(e);
      }
    }
  }
};
