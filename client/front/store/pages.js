import {modelBase} from './index';


export default modelBase({
  baseURL: 'page',
  order: 'sort ASC',
});
