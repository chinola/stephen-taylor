import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import colors from 'vuetify/es5/util/colors'

Vue.use(Vuetify, {
  theme: {
    primary: '#009975', // a color that is not in the material colors palette
    accent: '#007A5F',
    secondary: '#ED9400',
    info: colors.teal.lighten1,
    warning: colors.amber.base,
    error: '#E00023',
    success: colors.green.accent3
  }
})
