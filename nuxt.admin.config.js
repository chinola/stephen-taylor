const path = require('path');

const prod = (process.env.NODE_ENV === 'production');

// Grab env vars from .env file, if there
require('dotenv').config();

const cloudinaryID = process.env.CLOUDINARY_URL.split('@').pop();

export default {
  mode: 'spa',
  telemetry: false,

  /*
  ** Pass environment variables
  */
  env: {
    cloudinaryURL: `https://res.cloudinary.com/${cloudinaryID}/image/upload`,
  },

  /*
  ** Headers of the page
  */
  head: {
    titleTemplate: (chunk) => {
      const title = 'Admin';
      return chunk ? `${title} · ${chunk}` : title;
    },
    meta: [{
      charset: 'utf-8'
    }, {
      name: 'viewport', content: 'width=device-width, initial-scale=1'
    }],
    link: [{
      rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'
    }, {
      rel: 'stylesheet',
      href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
    }],
  },

  /*
  ** Set dirs
  */
  srcDir: 'client/admin',
  buildDir: '.nuxt/admin',
  generate: {
    dir: 'dist/admin/',
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Router config
  */
  router: {
    // middleware: ['auth'],
    linkExactActiveClass: 'active',
    base: '/admin/',
  },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '@/plugins/vuetify',
    {src: '@/plugins/vuedraggable', ssr: false},
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/vuetify',
    '@nuxtjs/toast',
  ],

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    https: prod,
    debug: !prod,
    baseURL: prod ? 'https://stephen-taylor.herokuapp.com/api' : 'http://localhost:3000/api',
  },

  /*
  ** Auth module config
  */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `/user/login/`,
            method: 'post',
            propertyName: 'id'
          },
          logout: {
            url: `/user/logout/`,
            method: 'post'
          },
          user: {
            url: `/user/me/`,
            method: 'get',
            propertyName: false
          },
        },
        tokenType: false,
      },
    },
    redirect: {
      login: '/login/',
      logout: '/login/',
      callback: false,
      home: '/',
    },
    cookie: {
      path: '/admin/',
      expires: new Date(new Date().getTime() + (8*60*60*1000)),  // in 8 hours
      secure: prod,
    },
    resetOnError: true,
  },

  /*
  ** Toast module config
  */
  toast: {
    position: 'bottom-center',
    theme: 'toasted-primary',
  },

  /*
  ** Build configuration
  */
  build: {
    transpile: ['vuetify/lib'],
    extractCSS: true,
    publicPath: '/admin/_nuxt/',

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
