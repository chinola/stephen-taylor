const path = require('path');
const prod = (process.env.NODE_ENV === 'production');

// Grab env vars from .env file, if there
require('dotenv').config();

const cloudinaryID = process.env.CLOUDINARY_URL.split('@').pop();

export default {
  mode: 'universal',
  telemetry: false,

  /*
  ** Pass environment variables
  */
  env: {
    cloudinaryURL: `https://res.cloudinary.com/${cloudinaryID}/image/upload`,
  },

  /*
  ** Headers of the page
  */
  head: {
    htmlAttrs: {
      lang: 'en-GB',
      itemscope: '',
      itemtype: 'http://schema.org/WebPage',
    },
    titleTemplate: function(chunk) {
      const title = 'taylor the creator';
      return (chunk ? `${chunk} · ${title}` : title).toLowerCase();
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
    ]
  },

  /*
  ** Set nuxt dir
  */
  srcDir: 'client/front',
  buildDir: '.nuxt/front',

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#009975' },

  /*
  ** Router config
  */
  router: {
    // middleware: ['auth'],
    linkActiveClass: 'parent-active',
    linkExactActiveClass: 'active',
  },

  /*
  ** Global CSS
  */
  css: [],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
  ],

  /*
  ** Style Resources - global styles
  */
  styleResources: {
    scss: [
      '~/assets/style/_variables.scss',
    ],
  },

  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    https: prod,
    debug: !prod,
    prefix: process.env.API_PREFIX || '/api',
    host: process.env.HOST,
    port: process.env.PORT,
  },

  /*
  ** Auth module config
  */
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `/user/login/`,
            method: 'post',
            propertyName: 'id'
          },
          logout: {
            url: `/user/logout/`,
            method: 'post'
          },
          user: {
            url: `/user/me/`,
            method: 'get',
            propertyName: false
          },
        },
        tokenType: false,
      },
    },
    redirect: {
      login: '/login/',
      logout: '/login/',
      callback: false,
      home: '/admin/',
    },
    cookie: {
      path: '/admin/',
      expires: new Date(new Date().getTime() + (8*60*60*1000)),  // in 8 hours
      secure: prod,
    },
    resetOnError: true,
  },

  /*
  ** Toast module config
  */
  toast: {
    position: 'bottom-center',
    theme: 'toasted-primary',
  },

  /*
  ** Build configuration
  */
  build: {
    // transpile: ['vuetify/lib'],
    extractCSS: true,

    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  }
}
