import {Nuxt, Builder} from 'nuxt';

import loopback from 'loopback';
import boot from 'loopback-boot';
import explorer from 'loopback-component-explorer';

import adminConfig from './nuxt.admin.config';
import frontConfig from './nuxt.config';

// Grab env vars
require('dotenv').config();

const API = process.env.API_PREFIX || '/api';
const HOST = process.env.HOST;
const PORT = process.env.PORT;

const dev = !(process.env.NODE_ENV === 'production');


// Set API
const app = loopback();
boot(app, 'server');

// Build Nuxt
adminConfig.dev = frontConfig.dev = dev;
const nuxtAdmin = new Nuxt(adminConfig);
const nuxtFront = new Nuxt(frontConfig);

// Build dev environment
if (dev) {
  const adminBuilder = new Builder(nuxtAdmin);
  const frontBuilder = new Builder(nuxtFront);
  adminBuilder.build();
  frontBuilder.build();
  const mountPath = `${API}/explorer`;
  explorer(app, {basePath: API, mountPath});
  console.log(`Explore your REST API at http://${HOST}:${PORT}${mountPath}`);
}

// Use loopback for all requests starting with `API`
app.all(new RegExp(`^${API}.*`), app.loopback.Router());

// Use Nuxt renderer for other requests
app.all(new RegExp(`^(?!${API})(?!/admin).*`), nuxtFront.render);
app.all(new RegExp(`^(?!${API})/admin.*`), nuxtAdmin.render);

// Start the app
app.listen(PORT, HOST, (err) => {
  if (err) console.error(err);
});

export default app;
