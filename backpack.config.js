module.exports = {
    webpack: (config, options, webpack) => {
        // change main entry
        config.entry.main = './index.js';
        // don't do nuxt
        config.module.rules.forEach((rule) => {
            rule.exclude.push(`${__dirname}/client/`);
        });
        return config;
    },
};
