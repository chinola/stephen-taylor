'use strict';

const slugify = require('transliteration').slugify;

module.exports = function(Model, options) {
  const required = options.required !== undefined ? options.required : true;
  Model.defineProperty('slug', {
    type: String,
    required: required,
    index: {
      unique: options.unique !== undefined ? options.unique : true,
    },
  });

  const getSlug = (data, field) => {
    if (Array.isArray(field)) {
      let slug = [];
      return field.forEach((val) => {
        slug.push(data[val]);
      });
      return slug.join(' ').trim();
    } else {
      return data[field];
    }
  };

  Model.observe('before save', (ctx, next) => {
    const emptyVals = [undefined, null, ''];
    if (ctx.instance) {
      if (emptyVals.includes(ctx.instance.slug) && !required) return next();
      const slug = getSlug(ctx.instance, options.field);
      if (!!slug) {
        ctx.instance.slug = slugify(
          slug, options
          );
        }
      } else {
      if (emptyVals.includes(ctx.data.slug) && !required) return next();
      const slug = getSlug(ctx.data, options.field);
      if (!!slug) {
        ctx.data.slug = slugify(getSlug(ctx.data, options.field), options);
      }
    }
    // TODO: Check if slug already exists, and then.. add a number to the end?
    return next();
  });
};
