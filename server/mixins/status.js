'use strict';


module.exports = function(Model, options = {}) {
  Model.defineProperty('status', Object.assign({
    type: Boolean,
    required: true,
    default: false,
  }, options));

  Model.observe('access', function(ctx, next) {
    const userId = ctx.options.accessToken ? ctx.options.accessToken.userId : false;
    if (!userId) {
      ctx.query.where = Object.assign(
        {}, ctx.query.where, {status: true}
      );
    }
    next();
  });
};
