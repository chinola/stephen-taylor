'use strict';

module.exports = function(server, cb) {
  // Add models to the database
  const db = server.dataSources.db;
  console.log('Auto updating tables');
  db.autoupdate((err) => {
    if (err) throw err;
    return cb()
  })
};
