'use strict';

module.exports = function(server, cb) {
  // Install a `/api/` route that returns server status
  var router = server.loopback.Router();
  router.get('/api/', server.loopback.status());
  server.use(router);
  return cb();
};
