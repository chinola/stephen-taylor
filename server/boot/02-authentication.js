'use strict';

const config = require('../../config.json');

module.exports = function(server, cb) {
  console.log('Setting up authentication');
  server.enableAuth();
  let User = server.models.ApiUser;
  User.count((err, count) => {
    if (err) throw err;
    if (count === 0) {
      User.create({
        name: config.admin.name,
        username: config.admin.username,
        email: config.admin.email,
        password: config.admin.password,
      }, (err, user) => {
        if (err) throw err;
        return cb();
      });
    } else {
      console.log('User already created');
      return cb();
    }
  });
}
