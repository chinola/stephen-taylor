'use strict';

const cloudinary = require('cloudinary');

module.exports = function(Media) {
  Media.delete = function(publicId, cb) {
    return cloudinary.v2.uploader.destroy(publicId, (err, resp) => {
      if (err) throw err;
      cb(resp);
    });
  };

  Media.remoteMethod('delete', {
    accepts: {arg: 'publicId', type: 'string'},
    responds: {arg: 'result', type: 'string'},
    http: {path: '/:publicId', verb: 'del', status: 204, source: 'path'},
  });
};
