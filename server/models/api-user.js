'use strict';


const validatePhone = require('../validators/phone-number');

module.exports = function(User) {
  User.validate('phone', validatePhone, {message: 'Invalid phone number'});
};
