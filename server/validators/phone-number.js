'use strict';

const PNF = require('google-libphonenumber').PhoneNumberFormat;

const phoneUtil = require('google-libphonenumber').PhoneNumberUtil.getInstance();

module.exports = function(err) {
  if (!this.phone) return;
  let number;
  try {
    number = phoneUtil.parse(this.phone, 'GB');
  } catch (e) {
    return err(null, e.message);
  }
  if (!phoneUtil.isValidNumber(number)) err();
};
