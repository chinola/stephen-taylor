module.exports = {
  db: {
    connector: 'postgresql',
    name: 'db',
    url: process.env.DATABASE_URL,
  },
  files: {
    name: 'files',
    connector: 'loopback-component-cloudinary',
    url: process.env.CLOUDINARY_URL
  }
};
